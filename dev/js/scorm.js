$(function() {
	window._wq = window._wq || [];

	var sco = pipwerks.SCORM;
	var connected = sco.init()

	function finishModule() {
		sco.status("set", "completed");
		sco.save()
		sco.quit()
	}

	if (connected) {
		$(".complete-module").on("click", function() {
			if (!$(this).hasClass("disabled")) {
				finishModule();
			}
		})
	}
	else {
		console.warn("There was an issue connecting to the SCORM API.")
	}

	_wq.push({ id: "s80rbsumc5", onReady: function(video) {
		video.bind("end", function() {
  			finishModule();
		});
	}});

	_wq.push({ id: "g2l8yg687r", onReady: function(video) {
		video.bind("end", function() {
  			finishModule();
		});
	}});
})