$(function() {
	var $sliderScreens = $(".slider-screen");

	$sliderScreens.each(function(i) {
		var $main = $(this).addClass("prev-disabled");
		var $crumbWrap = $main.find(".slider-crumbs");
		var $slides = $main.find(".slide");
		var $prev = $main.find(".slider-prev").css("opacity", 0).addClass("disabled");
		var $next = $main.find(".slider-next");

		initSliderCrumbs($slides.length, $crumbWrap);
		bindNavigation($prev, $next, $crumbWrap, $main);
	})

	function initSliderCrumbs(i, $wrap) {
		var $crumb = $("<i class='fa fa-circle slide-crumb'>");

		for (var x = 0; x < i; x++) {
			$wrap.append($crumb.clone())
		}

		$wrap.children().eq(0).addClass("c-active");
	}

	function bindNavigation($prev, $next, $crumbWrap, $main) {
		var hammer = new Hammer($main[0]);

		$prev.on("click", function() {
			if (!$(this).hasClass("disabled")) {
				unbindAll($prev, $next, $crumbWrap, hammer);
				var current = getCurrent($main);
				changeSlideTo(current - 1, $main, $crumbWrap, $prev, $next);
			}
		});

		$next.on("click", function() {
			if (!$(this).hasClass("disabled")) {
				unbindAll($prev, $next, $crumbWrap, hammer);
				var current = getCurrent($main);
				changeSlideTo(current + 1, $main, $crumbWrap, $prev, $next);
			}
		});

		$crumbWrap.find(".slide-crumb").on("click", function() {
			unbindAll($prev, $next, $crumbWrap, hammer);
			changeSlideTo($(this).index(), $main, $crumbWrap, $prev, $next);
		})

		hammer.on("swipe", function(e) {
			if (e.deltaX > 0 && !$main.hasClass("prev-disabled")) {
				unbindAll($prev, $next, $crumbWrap, hammer);
				var current = getCurrent($main);
				changeSlideTo(current - 1, $main, $crumbWrap, $prev, $next);
			}
			else if (e.deltaX < 0 && !$main.hasClass("next-disabled")) {
				unbindAll($prev, $next, $crumbWrap, hammer);
				var current = getCurrent($main);
				changeSlideTo(current + 1, $main, $crumbWrap, $prev, $next);
			}
		})
	}

	function unbindAll($prev, $next, $crumbWrap, hammer) {
		$prev.off("click");
		$next.off("click");
		$crumbWrap.find(".slide-crumb").off("click");
		hammer.off("swipe");
	}

	function getCurrent($main) { return $main.find(".active-slide").index() }

	function changeSlideTo(i, $main, $crumbWrap, $prev, $next) {
		var $active = $main.find(".active-slide");
		var $new = $main.find(".slide").eq(i);

		var $crumbs = $crumbWrap.children();
		var $activeCrumb = $crumbWrap.find(".c-active");
		var $newCrumb = $crumbs.eq(i);

		checkNew($new, $main);

		$active.fadeOut(500, function() {
			$new.fadeIn(function() {
				bindNavigation($prev, $next, $crumbWrap, $main);
			});
			swapClasses();
		})

		if ($active.index() > $new.index()) {
			$active.animate({left: "-15px"}, {queue: false, duration: 550, complete: function() {
				$active.css("left", "0")
			}});
		}
		else {
			$active.animate({left: "15px"}, {queue: false, duration: 550, complete: function() {
				$active.css("left", "0")
			}});
		}

		function swapClasses() {
			$active.removeClass("active-slide");
			$new.addClass("active-slide");
			$activeCrumb.removeClass("c-active");
			$newCrumb.addClass("c-active");
		}
	}

	function checkNew($new, $main) {
		if ($new.find("button").length > 0) {
			$main.addClass("next-disabled")
			$main.find(".slider-next").css("opacity", "0").addClass("disabled");
		}
		else {
			$main.removeClass("next-disabled")
			$main.find(".slider-next").css("opacity", "1").removeClass("disabled");
		}

		if ($new.index() == 0) {
			$main.addClass("prev-disabled")
			$main.find(".slider-prev").css("opacity", "0").addClass("disabled");
		}
		else {
			$main.removeClass("prev-disabled")
			$main.find(".slider-prev").css("opacity", "1").removeClass("disabled");
		}
	}
})