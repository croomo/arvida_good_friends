$(function() {
	var $hamburger = $(".hamburger");
	var $menu = $(".hamburger-menu");
	var $trigger = $(".trigger");
	var $content = $(".content");
	var submenuClick = false;

	function bindHamburger() {
		$hamburger.on("click", function() {
			$hamburger.off("click");
			
			$hamburger.toggleClass("open")
			$menu.toggleClass("open")

			if ($menu.hasClass("open")) {
				$content.addClass("scroll-lock");
				$menu.animate({top: 68}, {queue: false, duration: 500});
				$menu.fadeIn(500, function() {
					bindHamburger();
				});
			}
			else {
				$menu.animate({top: -68}, {queue: false, duration: 500})
				$menu.fadeOut(500, function() {
					$content.removeClass("scroll-lock");
					bindHamburger();
				});
			}
		})
	}

	function bindMenuItems() {
		$trigger.on("click", function(e) {
			var $this = $(this);
			$this.find("i").toggleClass("rotate")
			$this.next().slideToggle(500)
			$this.toggleClass("closed")
		})
	}

	bindHamburger();
	bindMenuItems();
})