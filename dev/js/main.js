$(function() {
	var $window = $(window);
	var $body = $("body");
	var $screens = $(".screen").hide();
	var $next = $(".next");
	var $back = $(".btn-back");
	var $branch = $(".btn[data-branch]").not(".btn-back");
	var $menu = $(".menu");
	var $content = $(".content");
	var $canScroll = $(".can-scroll-wrap");
	var starRating;
	var note;

	$($screens[0]).show().addClass("active");

	function resizeScreen() {
		$body.css("height", window.innerHeight);
		$body.css("width", window.innerWidth);
	}

	function checkMenu() {
		var $active = $content.find(".active");

		if ($active.hasClass("no-menu")) {
			$menu.fadeOut();
		}
		else {
			$menu.fadeIn();
		}
	}

	function bindApp($app) {
		var $appSlides = $app.children().not(".modal");
		var $appNext = $app.find(".app-next");
		var $checkIn = $app.find(".btn-check-in");
		var $contBtn = $app.find(".btn-continue");
		var $dismiss = $app.find(".btn-modal-close");

		$canScroll.hide();
		
		$dismiss.on("click", function() {
			$(this).parents(".modal").fadeOut()
		})

		$contBtn.parent().addClass("app-continue");
		$($appSlides.children()[0]).not(".modal").show().addClass("active");

		var $bubble = $($appSlides.children()[0]).find(".triangle-border").hide();

		setTimeout(function() {
			$bubble.fadeIn()
		}, 1250)

		$canScroll.fadeOut();
		
		$appNext.on("click", function() {
			var $active = $app.find(".sub-app-screen.active");

			$active.fadeOut(function() {
				$active.parent().scrollTop(0);
				var $bubble = $($appSlides.children()[0]).find(".triangle-border").hide();
				$active.removeClass("active");
				$active.next().fadeIn();
				$active.next().addClass("active");

				setTimeout(function() {
					$bubble.fadeIn()
				}, 1250);
			});
		});

		$checkIn.on("click", function() {
			$this = $(this);

			$this.removeClass("not-clicked");
			$this.prev().css("opacity", 0);
			$this.prev().animate({opacity: 1}, {queue: false, complete: function() {
				setTimeout(function() {
					$contBtn.parent().animate({bottom: 0})
				}, 500)
			}})
			$this.prev().slideDown(750);
			

			if ($(this).text() == "Check Out") {
				$(this).prev().text("You've checked out")
			}
		})
	}

	$next.on("click", function() {

		if (!$(this).hasClass("disabled")) {
			if ($(this).parents(".content-wrap").hasClass("only-text")) {
				var slide = $(this).parents(".content-wrap").parent()
				slide.removeClass("active").hide()
				slide.next().addClass('active').show()
			}
			else if ($(this).text() == "submit order") {
				parseSortable()
			}
			else {
				changeTo(getCurrentIndex() + 1)
			}
		}
	});

	$back.on("click", function() {
		var $this = $(this);
		var branch = $this.data("branch");

		if (!$this.hasClass("disabled")) {
			if ($(this).parents(".content-wrap").hasClass("only-text") && !$(this).parents(".content-wrap").hasClass("first")) {
				var slide = $(this).parents(".content-wrap").parent()
				slide.removeClass("active").hide()
				slide.prev().addClass('active').show()
			}
			else if (branch > 0) {
				changeTo(getCurrentIndex() - branch)
			}
			else {
				changeTo(getCurrentIndex() - 1)
			}
		}
	});

	$branch.on("click", function() {
		if (!$(this).hasClass("disabled"))
			changeTo(getCurrentIndex() + $(this).data("branch"))
	});

	function getCurrentIndex() {
		var $active = $content.find(".active");
		return $active.index();
	}

	function bindImageHotspots() {
		var $active = $content.find(".active");
		var $wrap = $active.find(".speech-bubble-wrap");
		var $img = $wrap.find("img");
		var $resize = $wrap.find(".resize-me");
		var $trigger = $wrap.find(".bubble-trigger");
		var $cont = $active.find(".btn-continue");

		if ($img.length > 0) {
			$wrap.find(".triangle-border[data-bubble]").hide();

			function resize() {
				$resize.each(function() {
					var calc;
					var left;
					var fontCalc;
					var $this = $(this);

					$wrap.removeAttr("style");
					$wrap.css("width", $img[0].offsetWidth)
					$wrap.css("height", $img[0].offsetHeight)
					$this.css("left", $this.data("left"));
					$this.css("bottom", $this.data("bottom"));

					if (!$this.hasClass("bubble-trigger")) {
						left = $this.position().left;
						leftCalc = ($img[0].naturalWidth - $img[0].offsetWidth) / 100 * 5;
						$this.css("left", left - leftCalc);

						fontCalc = ($img[0].naturalWidth - $img[0].offsetWidth) / 1000;
						$this.css("fontSize", 1.7 - fontCalc + "rem");
					}
				})
			}

			$window.on("resize", function() {
				resize();
			})

			function bindClicks() {
				$trigger.on("click", function() {
					$(this).removeClass("not-clicked");
					$wrap.find(".triangle-border").hide()
					$wrap.find(".triangle-border").eq($(this).data("show")).show()
					resize();
					checkUnlock();
				});
			}

			function checkUnlock() {
				if ($wrap.find(".not-clicked").length == 0) {
					$cont.removeClass("disabled");
				}
			}

			bindClicks();
			resize();
		}
	}

	function changeTo(i) {
		var $active = $content.find(".active");
		var $screens = $content.children();
		var $new = $($screens.eq(i));

		$active.fadeOut(function() {
			$('html, body').scrollTop(0);
			$active.removeClass("active");

			$new.fadeIn();
			$new.addClass("active");

			if ($new.children().hasClass("app-wrap")) {
				$canScroll.hide();
				bindApp($new);
				$new.children().not(".app-continue").animate({bottom: 15}, {queue: false, duration: 1000});

				if ($new.children().hasClass("prefill")) {
					$new.find(".stars").children().removeClass("fa-star-o fa-star");
					$new.find(".stars").children().slice(0, starRating + 1).addClass("fa-star");
					$new.find(".stars").children().not(".fa-star").addClass("fa-star-o");
					$new.find(".notes").html("<p>" + note + "</p>")
				}
			}

			if ($new.children().hasClass("star-wrap")) {
				$new.find("button").first().addClass("disabled");
				
				$(".interactive-star").on("click", function() {
					$(".interactive-star").removeClass("fa-star-o fa-star");
					$(".interactive-star").slice(0, $(this).index() + 1).addClass("fa-star");
					$(".interactive-star").not(".fa-star").addClass("fa-star-o");

					starRating = $(this).index();

					$new.find("button").first().removeClass("disabled");
				})
			}

			if ($new.children().hasClass("note-wrap")) {
				$new.find(".btn-quiz").on("click", function() {
					note = $(this).text()
				});
			}

			if ($new.children().hasClass("commit-wrap")) {
				var $button = $new.find("button");

				$button.addClass("disabled");

				$new.find("input").on("click", function() {
					if ($(this).is(":checked")) {
						$button.removeClass("disabled");
					}
					else {
						$button.addClass("disabled");
					}
				})
			}

			if ($new.find(".reorder-activity").length > 0) {
				bindReorderActivity($new);
			}

			checkMenu();
			bindImageHotspots();

			setTimeout(function() {
				if ($new[0].scrollHeight > window.innerHeight) {
					$canScroll.fadeIn();

					setTimeout(function() {
						$canScroll.fadeOut();
					}, 3000)
				}
				else {
					$canScroll.fadeOut();
				}
			}, 100)
		})
	}

	function bindReorderActivity($new) {
		var $nodes = $(".reorder-activity").find("ul");

		$nodes.sortable({
			containment: "parent",
			tolerance: "pointer",
			axis: "y",
			placeholder: "sortable-placeholder"
		});
	}

	function parseSortable() {
		var $nodes = $(".reorder-activity").find("ul");

		if ($nodes.children().eq(0).data("order") == 1) {
			if ($nodes.children().eq(1).data("order") == 2) {
				if ($nodes.children().eq(2).data("order") == 3) {
					changeTo(getCurrentIndex() + 2)
				}
				else {
					changeTo(getCurrentIndex() + 1)
				}
			}
			else {
				changeTo(getCurrentIndex() + 1)
			}
		}
		else {
			changeTo(getCurrentIndex() + 1)
		}
	}

	$window.on("resize", function() {
		resizeScreen();
	});

	resizeScreen();
	checkMenu();

	$("*").on("mouseup", function() {
    	$(this).blur()
    })
})


$(window).on("load", function() {
	$(".loader").fadeOut();
})